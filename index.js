var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

var promos = require('./promos');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8090;

app.use('/api', promos);
app.listen(port);