var express    = require('express');
var router     = express.Router();
var repository = require('./promosRepository');

router.get('/promos', function(req, res) {
    repository.getAll({
        success: function(models) {
            res.json({ 
                data: models
            });
        },
        failure: function(err) {
            res.status(500).json({ 
                data: err.toString() 
            });
        }
    })
});

router
.route('/promos/:promo_id')
.get(function(req, res) {
    repository.getById(req.params.promo_id, {
        success: function(model) {
            res.json({ 
                data: model
            });
        },
        failure: function(err) {
            res.status(500).json({ 
                data: err.toString() 
            });
        }
    });
});

module.exports = router;
